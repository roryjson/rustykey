mod subcommands {
    pub mod add;
    pub mod format;
    pub mod open;
}

use crate::subcommands::add::{add_luks_key, AddError};
use crate::subcommands::format::{format_luks_partition, FormatError};
use crate::subcommands::open::{open_luks, OpenError, UpdateSaltError};
use clap::{arg, command, Args, Parser, Subcommand, ValueEnum};
use log::LevelFilter;
use log::{debug, error, trace};
use rustykey::cryptsetup::recovery::recovery;
use rustykey::mount_wrapper::un_mount;
use rustykey::password_reader::{read_password_file, PasswordReadError};
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::path::PathBuf;
use yubico_manager::config::Slot;
use yubico_manager::config::Slot::{Slot1, Slot2};

/// Program to handle luks FDE
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[arg(long, short = 'v', action = clap::ArgAction::Count, global = true,)]
    verbose: u8,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Args, Debug)]
pub struct BaseArguments {
    /// path to cryptsetup executable
    #[arg(short = 'c', long, default_value = "cryptsetup")]
    cryptsetup: PathBuf,

    /// path to encrypted device
    #[arg(short = 'd', long)]
    device: PathBuf,

    /// Slot to use for yubikey challenge_response
    #[arg(short = 'y', long, value_enum)]
    yubikey_slot: ClapYubikeySlot,

    /// If set don't ask for password, just use yubikey to generate key file
    #[arg(long = "1FA")]
    one_f_a: bool,

    /// Length of the new salt in byte (64 is the effective maximum).
    #[arg(long, default_value = "16")]
    salt_length: u8, // todo maybe constrain to max 64 in future

    /// Length of the LUKS slot key derived with PBKDF2 in byte.
    #[arg(long, default_value = "64")]
    key_length: usize,

    /// path to salt iterations file
    #[arg(short = 'f', long)]
    salt_iter_file: PathBuf,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Open(OpenArgs),
    Add(AddArgs),
    Format(FormatArgs),
}

#[derive(Parser, Debug)]
struct OpenArgs {
    #[command(flatten)]
    base_args: BaseArguments,

    /// opens device and sets up a mapping to name given, passed to "luksOpen"
    #[arg(short = 'n', long)]
    name: String,

    /// Store password in a file to be attempted to reuse for next device
    #[arg(long)]
    reuse_password: bool,

    #[command(flatten)]
    mount: rustykey::mount_wrapper::CliMount,

    /// Used to specify we are running in the initrd, used to workaround some issues I face while running in initramfs or initrd
    #[arg(long)]
    in_initrd: bool,

    #[clap(raw = true)]
    crypsetup_args: Vec<String>,
}

#[derive(Parser, Debug)]
struct AddArgs {
    #[command(flatten)]
    base_args: BaseArguments,
}

#[derive(Parser, Debug)]
struct FormatArgs {
    #[arg(long)]
    password_file: PathBuf,

    #[clap(raw = true)]
    crypsetup_args: Vec<String>,

    #[command(flatten)]
    base_args: BaseArguments,
}

/// Doc comment
#[derive(ValueEnum, Clone, Debug)]
enum ClapYubikeySlot {
    /// Slot 1 of yubikey
    #[value(alias("1"))]
    Slot1,
    /// Slot 2 of yubikey
    #[value(alias("2"))]
    Slot2,
}

impl From<ClapYubikeySlot> for Slot {
    fn from(val: ClapYubikeySlot) -> Self {
        match val {
            ClapYubikeySlot::Slot1 => Slot1,
            ClapYubikeySlot::Slot2 => Slot2,
        }
    }
}

fn main() {
    let args = Cli::parse();
    match args.verbose {
        0 => {
            env_logger::init();
        }
        1 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Warn)
                .init();
        }
        2 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Info)
                .init();
        }
        3 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Debug)
                .init();
        }
        4 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Trace)
                .init();
        }
        5_u8..=u8::MAX => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Trace)
                .init();
        }
    }

    match &args.command {
        Commands::Add(add_args) => {
            if let Some(add_error) = add_luks_key(&add_args.base_args).err() {
                match add_error {
                    AddError::AddKey(_) => {
                        error!("Failed to add key to LUKS, exiting...")
                    }
                    AddError::KeyGenerationYubikey(_) => {
                        error!("Failed getting response from yubikey, key not added to LUKS");
                    }
                    AddError::SaveKeyError(_) => {
                        error!("Unable to save salt_iter file");
                    }
                }
            }
        }
        Commands::Open(open_args) => {
            if let Some(open_error) = open_luks(
                &open_args.base_args,
                &open_args.name,
                &open_args.mount,
                open_args.reuse_password,
                open_args.in_initrd,
                &open_args.crypsetup_args,
            )
            .err()
            {
                match open_error {
                    OpenError::Mount(_) | OpenError::Unlock(_) => recovery(
                        &open_args.base_args.cryptsetup,
                        &open_args.base_args.device,
                        &open_args.name,
                        open_args.in_initrd,
                        &open_args.crypsetup_args,
                    ),
                    OpenError::KeyGenerationSaltRetrieval(error) => {
                        debug!("salt retrieval error: {:?}", error);
                        error!("There was an issue retrieving salt/iterations");
                        recovery(
                            &open_args.base_args.cryptsetup,
                            &open_args.base_args.device,
                            &open_args.name,
                            open_args.in_initrd,
                            &open_args.crypsetup_args,
                        )
                    }
                    OpenError::KeyGenerationYubikey(_) => {
                        error!("There was an issue getting response from Yubikey");
                        recovery(
                            &open_args.base_args.cryptsetup,
                            &open_args.base_args.device,
                            &open_args.name,
                            open_args.in_initrd,
                            &open_args.crypsetup_args,
                        )
                    }
                    OpenError::UpdateSalt(salt_error) => match salt_error {
                        UpdateSaltError::KeyGenerationYubikey(_) => {
                            error!("Call to yubikey failed when generating next boot key, next boot will reuse old salt.");
                        }
                        UpdateSaltError::RenameNewCurrentOld(_) => {
                            // todo probably want a nicer error message
                            error!("New salt generated and used to replace luks key, however failed to move new salt file to specified location. This will prevent correct unlock on next boot");
                            // todo recovery atm is no use here but later it could have a feature to try again to move file
                            // todo consider commenting out for MVP
                            recovery(
                                &open_args.base_args.cryptsetup,
                                &open_args.base_args.device,
                                &open_args.name,
                                open_args.in_initrd,
                                &open_args.crypsetup_args,
                            )
                        } // breaks next boot
                        UpdateSaltError::CreateNewSalt(_) => {
                            error!("Failed to generate new salt, next boot will reuse old salt. No user intervention is needed");
                        }
                        UpdateSaltError::ChangeKey(_) => {
                            error!("Luks failed to update key, next boot will reuse old salt. No user intervention needed");
                        }
                    },
                    OpenError::Unmount(_) => {
                        error!("Failed to unmount device with salt on, this may prevent boot");
                    }
                }
            }
            un_mount(&open_args.mount).unwrap_or(()); // todo for now im ignoring the error as its likely the second call but I can do better

            // todo consider moving to bellow when nix updates over rust 1.65.0
            /* let Some(open_error) =  open_luks(
                &open_args.cryptsetup,
                &open_args.device,
                &open_args.name,
                &open_args.salt_iter_file,
                &open_args.storage_device,
                &open_args.mount_point,
            )
            .err() else { return; } ;

            match open_error {
                // use match statement from above
            }*/
        }
        Commands::Format(format_args) => {
            let r_password = match &format_args.password_file.to_str().unwrap_or("").eq("-") {
                true => {
                    trace!("password file was - reading from stdin");
                    read_password_file(&mut BufReader::new(io::stdin()))
                }
                false => {
                    trace!("Reading password file");
                    match File::open(&format_args.password_file) {
                        Ok(file) => read_password_file(&mut BufReader::new(file)),
                        Err(error) => Err(error.into()),
                    }
                }
            };
            let password = match r_password {
                Ok(password) => Some(password),
                Err(error) => match error {
                    PasswordReadError::Unknown => None,
                    PasswordReadError::InvalidChars(_) => None, // todo inform user that it has to be valid utf8 chars
                    PasswordReadError::IoError(_) => None,
                },
            };

            if let Some(format_error) = format_luks_partition(
                &format_args.base_args,
                password,
                &format_args.crypsetup_args,
            )
            .err()
            {
                match format_error {
                    FormatError::FormatPartition(error) => {
                        println!(
                            "Failed to format partition with the following error {:?}",
                            error
                        );
                        panic!();
                    }
                    FormatError::KeyGenerationYubikey(_) => {
                        panic!("Failed reading yubikey");
                    }
                    FormatError::PasswordFailure => {
                        panic!("Failed reading password");
                    }
                    FormatError::SaltIterSaveError(value) => {
                        panic!("Unable to save generated salt_iterations in specified file, partition was not formatted. IO error {}", Into::<io::Error>::into(value));
                    }
                }
            }
        }
    }
}
