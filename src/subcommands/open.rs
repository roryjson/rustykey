use crate::subcommands::open::CrypsetupError::IncorrectKey;
use crate::subcommands::open::OpenError::{
    KeyGenerationSaltRetrieval, KeyGenerationYubikey, Mount, Unlock, Unmount, UpdateSalt,
};
use crate::BaseArguments;
use inquire::{Password, PasswordDisplayMode};
use log::{debug, error, trace};
use rustykey::cryptsetup_caller::CrypsetupError;
use rustykey::key_helpers::{generate_salt_hex, KeyGenerator, SaltIterRetrievalError};
use rustykey::mount_wrapper::{mount_storage_device, un_mount, CliMount, MountError, UnMountError};
use rustykey::{cryptsetup_caller, yubikey};
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::{fs, io};

pub enum OpenError {
    Mount(MountError),
    Unlock(CrypsetupError),
    KeyGenerationSaltRetrieval(SaltIterRetrievalError),
    KeyGenerationYubikey(yubikey::Error),
    UpdateSalt(UpdateSaltError),
    Unmount(UnMountError),
}

impl From<MountError> for OpenError {
    fn from(value: MountError) -> Self {
        Mount(value)
    }
}
impl From<UnMountError> for OpenError {
    fn from(value: UnMountError) -> Self {
        Unmount(value)
    }
}

impl From<CrypsetupError> for OpenError {
    fn from(value: CrypsetupError) -> Self {
        Unlock(value)
    }
}

impl From<SaltIterRetrievalError> for OpenError {
    fn from(value: SaltIterRetrievalError) -> Self {
        KeyGenerationSaltRetrieval(value)
    }
}

impl From<yubikey::Error> for OpenError {
    fn from(value: yubikey::Error) -> Self {
        KeyGenerationYubikey(value)
    }
}
impl From<UpdateSaltError> for OpenError {
    fn from(value: UpdateSaltError) -> Self {
        UpdateSalt(value)
    }
}

pub fn open_luks(
    base_args: &BaseArguments,
    name: &str,
    mount_options: &CliMount,
    reuse_password: bool,
    in_initrd: bool,
    cryptsetup_args: &[String],
) -> Result<(), OpenError> {
    debug!("open_luks with  cryptsetup_executable_path <{:?}>, device <{:?}>, name <{name}>, salt_iter_path <{:?}>", base_args.cryptsetup, base_args.device, base_args.salt_iter_file);

    mount_storage_device(mount_options)?;

    let mut last_unlock_attempt: Option<Result<(), CrypsetupError>> = None; // none if never attempted some(result of unlock)

    if reuse_password {
        let mut key_gen = KeyGenerator::new_from_salt_iter_file(&base_args.salt_iter_file)?
            .hash_salt()
            .challenge_yubikey(base_args.yubikey_slot.clone().into())?;
        debug!("Attempting to reuse password");

        let user_password: Option<String> = fs::read_to_string("/crypt-ramfs/passphrase").ok();
        if let Some(user_password) = user_password {
            key_gen = key_gen.add_user_input(user_password.clone());
            // call drive key
            let unlock_key = key_gen.derive_key(base_args.key_length);

            // attempt to open luks partition
            last_unlock_attempt = Some(cryptsetup_caller::open(
                unlock_key.as_bytes(),
                &base_args.cryptsetup,
                &base_args.device,
                name,
                in_initrd,
                cryptsetup_args,
            ));
            if let Some(Ok(())) = last_unlock_attempt {
                update_salt(
                    base_args,
                    Some(user_password),
                    unlock_key,
                    &base_args.salt_iter_file,
                    in_initrd,
                    cryptsetup_args,
                )?;
            }
        }
    }

    trace!("compute ask_for_password, false = all good continue, true = ask for password again, Err on unrecoverable errors");
    let ask_for_password = match last_unlock_attempt {
        None => {
            trace!("unlock never called ask for password");
            true
        }
        Some(result_of_unlock_attempt) => {
            match result_of_unlock_attempt {
                Ok(_) => {
                    trace!("Successful unlock skip asking for password");
                    false
                }
                Err(error) => match error {
                    CrypsetupError::WrongParameter => {
                        error!("Wrong parameters passed to cryptsetup, this is an unrecoverable bug in the binary"); // should maybe add some debug stuff and ask user to make an issue
                        return Err(Unlock(error));
                    }
                    IncorrectKey => {
                        trace!("Last key was incorrect ask for new password");
                        true
                    }
                    CrypsetupError::OutOfMemory => {
                        trace!("out of memory error, retry I guess. ask for new password");
                        true
                    }
                    CrypsetupError::WrongDevice => false,
                    CrypsetupError::DeviceAlreadyExistsOrBusy => {
                        // exists should mean false, but idk what busy means atm
                        trace!("Device busy or already mounted, until I understand what busy means we shall retry. ask for new password");
                        true
                    }
                    CrypsetupError::Unknown => {
                        trace!("Unexpected/unknown error. ask for new password");
                        true
                    }
                    CrypsetupError::NoStatusCode => {
                        trace!("Failed to get status code. ask for new password");
                        true
                    }
                    CrypsetupError::WritingKey(_) => {
                        trace!("Failed to write key, likely to fail again. ask for new password anyway");
                        true
                    }
                    CrypsetupError::PathBufNone => {
                        trace!("Incorrect device given, exit");
                        return Err(Unlock(error));
                    }
                    CrypsetupError::BadPipe => {
                        trace!("Currently shouldn't happen in open");
                        return Err(Unlock(error));
                    }
                    CrypsetupError::Command(_) => {
                        trace!("Failed to spawn command, ask for new password");
                        true
                    }
                },
            }
        }
    };

    if ask_for_password {
        let mut key_gen = KeyGenerator::new_from_salt_iter_file(&base_args.salt_iter_file)?
            .hash_salt()
            .challenge_yubikey(base_args.yubikey_slot.clone().into())?;
        let mut user_password = None;
        if !base_args.one_f_a {
            debug!("Asking user for password");
            let input = Password::new("Please enter password:")
                .with_display_toggle_enabled()
                .without_confirmation()
                .with_display_mode(PasswordDisplayMode::Hidden)
                .with_formatter(&|_| String::from("Input received"))
                .prompt();

            user_password = match input {
                Ok(val) => {
                    key_gen = key_gen.add_user_input(val.clone());
                    Some(val)
                }
                Err(_) => todo!(),
            };
            if reuse_password {
                // save to file
            }
        }

        let key1 = key_gen.derive_key(base_args.key_length);

        trace!("key generated: {:02x?}", key1);

        cryptsetup_caller::open(
            key1.as_bytes(),
            &base_args.cryptsetup,
            &base_args.device,
            name,
            in_initrd,
            cryptsetup_args,
        )?;
        update_salt(
            base_args,
            user_password.clone(),
            key1,
            &base_args.salt_iter_file,
            in_initrd,
            cryptsetup_args,
        )?;
        if reuse_password {
            let mut output = File::create("/crypt-ramfs/passphrase").unwrap();
            output.write_all(user_password.unwrap().as_bytes()).unwrap();
        }
    }

    // todo move this out so an error doesnt prevent unmount, maybe in unmount it can be called repeatedly safely so I can call here and further up
    un_mount(mount_options)?;
    // block_utils::unmount_device(mount_options).map_err(Unmount)?;
    Ok(())
}

pub enum UpdateSaltError {
    KeyGenerationYubikey(yubikey::Error),
    /// error during renaming current keys, this will break next boot inform user
    RenameNewCurrentOld(io::Error),
    // error during renaming new key (.new) to current key, this will break next boot inform user
    // RenameNewCurrent(io::Error),
    /// Error when creating/writing new salt file, inform user salt was not updated, this won't break next boot.
    CreateNewSalt(io::Error),
    /// Error updating key in luks
    ChangeKey(CrypsetupError),
}

impl From<yubikey::Error> for UpdateSaltError {
    fn from(value: yubikey::Error) -> Self {
        UpdateSaltError::KeyGenerationYubikey(value)
    }
}
impl From<CrypsetupError> for UpdateSaltError {
    fn from(value: CrypsetupError) -> Self {
        UpdateSaltError::ChangeKey(value)
    }
}

fn to_rename_error(io_error: io::Error) -> UpdateSaltError {
    UpdateSaltError::RenameNewCurrentOld(io_error)
}

fn to_new_salt_error(io_error: io::Error) -> UpdateSaltError {
    UpdateSaltError::CreateNewSalt(io_error)
}

fn update_salt(
    base_args: &BaseArguments,
    user_password: Option<String>,
    existing_key: String, // I could compute this again...
    salt_iter_file: &PathBuf,
    in_initrd: bool,
    cryptsetup_args: &[String],
) -> Result<(), UpdateSaltError> {
    let salt = generate_salt_hex(base_args.salt_length);
    let iterations = 1000;
    // todo move to fn
    let mut salt_iter_file_new = salt_iter_file.clone();
    salt_iter_file_new.set_extension("new");
    let mut output = File::create(&salt_iter_file_new).map_err(to_new_salt_error)?;
    write!(output, "{salt}\n{iterations}").map_err(to_new_salt_error)?;
    drop(output);

    let mut key_gen = KeyGenerator::new_from_iterations(iterations)
        .add_salt_hex(salt.clone())
        .hash_salt()
        .challenge_yubikey(base_args.yubikey_slot.clone().into())?;

    if let Some(user_password) = user_password {
        key_gen = key_gen.add_user_input(user_password);
    }
    let new_key = key_gen.derive_key(base_args.key_length);

    cryptsetup_caller::change_key(
        existing_key.as_bytes(),
        new_key.as_bytes(),
        &base_args.cryptsetup,
        &base_args.device,
        in_initrd,
        cryptsetup_args,
    )?;

    let mut salt_iter_file_old = salt_iter_file.clone();
    salt_iter_file_old.set_extension("old"); //shall keep it around for recovery?? could result in 3 files
    fs::rename(salt_iter_file, &salt_iter_file_old).map_err(to_rename_error)?;
    fs::rename(&salt_iter_file_new, salt_iter_file).map_err(to_rename_error)?;

    Ok(())
}
