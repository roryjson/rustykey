use crate::BaseArguments;
use inquire::{Password, PasswordDisplayMode};
use log::trace;
use rustykey::cryptsetup_caller::CrypsetupError;
use rustykey::key_helpers::{
    generate_salt_hex, save_salt_iter_to_file, KeyGenerator, SaltIter, SaltIterSaveError,
};
use rustykey::{cryptsetup_caller, yubikey};

pub enum FormatError {
    FormatPartition(CrypsetupError),
    KeyGenerationYubikey(yubikey::Error),
    PasswordFailure,
    SaltIterSaveError(SaltIterSaveError),
}

impl From<SaltIterSaveError> for FormatError {
    fn from(value: SaltIterSaveError) -> Self {
        FormatError::SaltIterSaveError(value)
    }
}

impl From<CrypsetupError> for FormatError {
    fn from(value: CrypsetupError) -> Self {
        FormatError::FormatPartition(value)
    }
}

impl From<yubikey::Error> for FormatError {
    fn from(value: yubikey::Error) -> Self {
        FormatError::KeyGenerationYubikey(value)
    }
}

pub fn format_luks_partition(
    base_args: &BaseArguments,
    password: Option<String>,
    cryptsetup_args: &[String],
) -> Result<(), FormatError> {
    // if 1fa and password set warn user password is unused

    let salt = generate_salt_hex(base_args.salt_length);
    let iterations = 1000;

    let mut key_gen = KeyGenerator::new_from_iterations(iterations)
        .add_salt_hex(salt.clone())
        .hash_salt()
        .challenge_yubikey(base_args.yubikey_slot.clone().into())
        .unwrap();

    if !base_args.one_f_a {
        match password {
            None => {
                let input = Password::new("Please enter new password:")
                    .with_display_toggle_enabled()
                    .with_display_mode(PasswordDisplayMode::Hidden)
                    .with_formatter(&|_| String::from("Input received"))
                    .prompt();

                let user_password = match input {
                    Ok(val) => val,
                    Err(_) => return Err(FormatError::PasswordFailure),
                };
                key_gen = key_gen.add_user_input(user_password);
            }
            Some(password) => key_gen = key_gen.add_user_input(password),
        }
    }

    let key = key_gen.derive_key(base_args.key_length);
    trace!("key generated: {:02x?}", key);

    save_salt_iter_to_file(&base_args.salt_iter_file, &SaltIter { salt, iterations })?;

    cryptsetup_caller::format_partition(
        key.as_bytes(),
        &base_args.cryptsetup,
        &base_args.device,
        cryptsetup_args,
    )?;

    Ok(())
}
