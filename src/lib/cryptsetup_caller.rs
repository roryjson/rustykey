use crate::cryptsetup_caller::CrypsetupError::{BadPipe, PathBufNone};
use log::{debug, error, trace, warn};
use std::fs::File;
use std::io::{self, BufWriter, Write};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::{fs, process};

// Error codes are: 1 wrong parameters, 2 no permission (bad
//        passphrase), 3 out of memory, 4 wrong device specified, 5 device
//        already exists or device is busy.
// todo consider capturing the stderr output
#[derive(Debug)]
pub enum CrypsetupError {
    /// incorrect parameters passed to cryptsetup, if this happens it is a bug
    WrongParameter,
    /// An incorrect key was passed to cryptsetup, the device isn't unlocked
    IncorrectKey,
    // idk what to do here
    OutOfMemory,
    /// incorrect device specified
    WrongDevice,
    /// Device might already be unlocked
    DeviceAlreadyExistsOrBusy,
    Unknown,
    NoStatusCode,
    WritingKey(io::Error),
    PathBufNone,
    /// Unable to take pipe to send data to stdin
    BadPipe,
    Command(io::Error),
}

fn to_writing_key_error(io_error: io::Error) -> CrypsetupError {
    CrypsetupError::WritingKey(io_error)
}

fn to_command_error(io_error: io::Error) -> CrypsetupError {
    CrypsetupError::Command(io_error)
}

fn handle_cryptsetup_result(
    command_result: Result<process::Output, io::Error>,
) -> Result<(), CrypsetupError> {
    let dest_output = command_result.map_err(|err| {
        warn!("Internal error {}", err);
        CrypsetupError::Unknown
    })?;
    match dest_output.status.code() {
        Some(0) => {
            // println!("OK: {}", String::from_utf8_lossy(&dest_output.stdout));
            debug!(
                "Successful call output was {}",
                String::from_utf8_lossy(&dest_output.stdout)
            );
            Ok(())
        }
        Some(code) => {
            warn!("cryptsetup error {:?}", dest_output.stderr);
            // error!("Unable to unlock device with generated key.");
            match code {
                1 => Err(CrypsetupError::WrongParameter),
                2 => Err(CrypsetupError::IncorrectKey),
                3 => Err(CrypsetupError::OutOfMemory),
                4 => Err(CrypsetupError::WrongDevice),
                5 => Err(CrypsetupError::DeviceAlreadyExistsOrBusy),
                _ => Err(CrypsetupError::Unknown),
            }
        }
        None => {
            error!("Got no status from cryptsetup command");
            Err(CrypsetupError::NoStatusCode)
        }
    }
}

pub fn format_partition(
    new_key: &[u8],
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
    additional_args: &[String],
) -> Result<(), CrypsetupError> {
    let mut cryptsetup_format = Command::new(cryptsetup_executable_path)
        .args(["luksFormat", device.to_str().ok_or(PathBufNone)?])
        .args(["--batch-mode"]) // skips password confirmation
        .args(["--key-file=-"])
        .args(additional_args)
        // this wont be called in initrd so we can use pipes
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .map_err(to_command_error)?;

    let mut writer = BufWriter::new(cryptsetup_format.stdin.take().ok_or(BadPipe)?);

    writer.write_all(new_key).map_err(to_writing_key_error)?;
    writer.flush().map_err(to_writing_key_error)?;
    drop(writer);

    handle_cryptsetup_result(cryptsetup_format.wait_with_output())
}

pub fn add_key(
    existing_key: String, // todo make &[u8] to be consistent with open function
    new_key: &[u8],
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
) -> Result<(), CrypsetupError> {
    let mut cryptsetup_add = Command::new(cryptsetup_executable_path)
        .args(["luksAddKey", device.to_str().ok_or(PathBufNone)?])
        .args(["--key-file=-"])
        // both old and new key in same stream/file we need to tell cryptsetup where the old key ends and new keys starts
        .args([format!("--keyfile-size={}", existing_key.as_bytes().len())])
        // this wont be called in initrd so we can use pipes
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .map_err(to_command_error)?;

    let mut writer = BufWriter::new(cryptsetup_add.stdin.take().ok_or(BadPipe)?);

    writer
        .write_all(existing_key.as_bytes())
        .map_err(to_writing_key_error)?;
    writer.write_all(new_key).map_err(to_writing_key_error)?;
    writer.flush().map_err(to_writing_key_error)?;
    drop(writer);

    handle_cryptsetup_result(cryptsetup_add.wait_with_output())
}

pub fn open(
    key: &[u8],
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
    name: &str,
    in_initrd: bool,
    additional_args: &[String],
) -> Result<(), CrypsetupError> {
    let mut cryptsetup_open_builder = Command::new(cryptsetup_executable_path);
    cryptsetup_open_builder
        .args(["luksOpen", device.to_str().ok_or(PathBufNone)?, name])
        .stdout(Stdio::piped());

    match in_initrd {
        true => {
            // annoyingly atm I can't pipe in initrd todo solve this!
            debug!("In initrd, save luks key to file");
            let path = "pass-file.luks";
            let mut output = File::create(path).map_err(to_writing_key_error)?;
            output.write_all(key).map_err(to_writing_key_error)?;

            debug!("Finish constructing cryptsetup_open command");
            let cryptsetup_open = cryptsetup_open_builder
                .args(["--key-file=pass-file.luks"])
                .args(additional_args)
                // .stdin(Stdio::piped())
                .spawn()
                .map_err(to_command_error)
                .map_err(|e| {
                    delete_key_file(path);
                    e
                })?;
            handle_cryptsetup_result(cryptsetup_open.wait_with_output()).map(|e| {
                delete_key_file(path);
                e
            })
        }
        false => {
            let mut cryptsetup_open = cryptsetup_open_builder
                .args(["--key-file=-"])
                .args(additional_args)
                .stdin(Stdio::piped())
                .spawn()
                .map_err(to_command_error)?;

            let mut writer = BufWriter::new(cryptsetup_open.stdin.take().ok_or(BadPipe)?);

            writer.write_all(key).map_err(to_writing_key_error)?;
            writer.flush().map_err(to_writing_key_error)?;
            drop(writer);
            handle_cryptsetup_result(cryptsetup_open.wait_with_output())
        }
    }
}

pub fn change_key(
    existing_key: &[u8],
    new_key: &[u8],
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
    in_initrd: bool,
    additional_args: &[String],
) -> Result<(), CrypsetupError> {
    debug!("Start constructing change key command");
    let mut cryptsetup_change_key_builder = Command::new(cryptsetup_executable_path);
    cryptsetup_change_key_builder
        .args(["luksChangeKey", device.to_str().ok_or(PathBufNone)?])
        .stdout(Stdio::piped());

    match in_initrd {
        true => {
            // annoyingly atm I can't pipe in initrd todo solve this!
            debug!("In initrd, generate and write current pass key file");
            let path_old = "pass-file.luks";
            let mut output_old = File::create(path_old).map_err(to_writing_key_error)?;
            output_old
                .write_all(existing_key)
                .map_err(to_writing_key_error)?;
            drop(output_old);

            debug!("Generate and write new pass key file");
            let path_new = "pass-file-new.luks";
            let mut output_new = File::create(path_new)
                .map_err(to_writing_key_error)
                .map_err(|e| {
                    delete_key_file(path_old);
                    e
                })?;
            output_new
                .write_all(new_key)
                .map_err(to_writing_key_error)
                .map_err(|e| {
                    delete_key_file(path_old);
                    e
                })?;
            drop(output_new);

            debug!("Finish constructing change key command");
            let cryptsetup_change_key = cryptsetup_change_key_builder
                .args(["pass-file-new.luks", "--key-file=pass-file.luks"])
                .args(additional_args)
                .stdout(Stdio::piped())
                .spawn()
                .map_err(to_command_error)
                .map_err(|e| {
                    delete_key_file(path_old);
                    delete_key_file(path_new);
                    e
                })?;

            debug!("Calling Cryptsetup to change key and handling result");
            handle_cryptsetup_result(cryptsetup_change_key.wait_with_output()).map(|e| {
                delete_key_file(path_old);
                delete_key_file(path_new);
                e
            })
        }
        false => {
            debug!("Finish constructing change key command");
            let mut cryptsetup_change_key = cryptsetup_change_key_builder
                .args([format!("--keyfile-size={}", existing_key.len())])
                .args(additional_args)
                .stdin(Stdio::piped())
                .spawn()
                .map_err(to_command_error)?;

            debug!("write existing key to stdin");
            let mut writer = BufWriter::new(cryptsetup_change_key.stdin.take().ok_or(BadPipe)?);
            writer
                .write_all(existing_key)
                .map_err(to_writing_key_error)?;
            debug!("Write new key to stdin");
            writer.write_all(new_key).map_err(to_writing_key_error)?;
            writer.flush().map_err(to_writing_key_error)?;
            drop(writer);

            debug!("Calling Cryptsetup to change key and handling result");
            handle_cryptsetup_result(cryptsetup_change_key.wait_with_output())
        }
    }
}

fn delete_key_file(path: &str) {
    debug!("deleting key file: {}", path);
    match fs::remove_file(path) {
        Ok(_) => {
            trace!("Successful delete")
        }
        Err(_) => {
            error!("Failed to delete new luks file")
        }
    }
}
