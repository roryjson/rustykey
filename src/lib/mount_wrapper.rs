use log::{debug, error};
use std::io;
use std::path::PathBuf;

use clap::{arg, Args};
use sys_mount::{unmount, Mount, UnmountFlags};

#[derive(Args, Debug)]
pub struct CliMount {
    /// Path to block device to mount for salt, requires mount-point to be set
    #[arg(short = 's', long, requires = "mount", group = "storage")]
    storage_device: Option<PathBuf>,

    /// Path to mount block device to mount for salt, requires storage-device to be set
    #[arg(short = 'm', long, requires = "storage", group = "mount")]
    mount_point: Option<PathBuf>,

    /// Optional hint for filesystem type this cn be helpful when in environments mount cant auto detect fstype, requires storage-device & mount-point to be set
    #[arg(long, requires_all = ["mount", "storage"])]
    mount_fstype: Option<String>,
}
#[derive(Debug)]
pub enum MountError {
    FailedToFindMountDevice,
    FailedToMount,
    InvalidOptions,
}

impl From<OptionsError> for MountError {
    fn from(_: OptionsError) -> Self {
        Self::InvalidOptions
    }
}
#[derive(Debug)]

pub enum UnMountError {
    ErrorUnmouning(io::Error),
    InvalidOptions,
}

impl From<OptionsError> for UnMountError {
    fn from(_: OptionsError) -> Self {
        Self::InvalidOptions
    }
}

fn io_to_unmount_error(io_error: io::Error) -> UnMountError {
    UnMountError::ErrorUnmouning(io_error)
}

pub enum OptionsError {
    StorageSetMountMissing,
    MountSetStorageMissing,
}

pub fn mount_storage_device(mount_options: &CliMount) -> Result<(), MountError> {
    let mount_result = match validate_mount_options(mount_options)? {
        None => {
            debug!("No mount option Skipping mount");
            return Ok(());
        }
        Some(storage_mount) => match &mount_options.mount_fstype {
            Some(fstype) => Mount::builder()
                .fstype(fstype.as_str())
                .mount(storage_mount.storage, storage_mount.mount),
            None => Mount::builder().mount(storage_mount.storage, storage_mount.mount),
        },
    };

    match mount_result {
        Ok(_) => Ok(()),
        Err(why) => {
            error!(
                "tried mounting {:?} to {:?}, with fs {:?}",
                mount_options.storage_device, mount_options.mount_point, mount_options.mount_fstype
            );
            error!("failed to mount device: {}", why);
            Err(MountError::FailedToMount)
        }
    }
}

pub fn un_mount(mount_options: &CliMount) -> Result<(), UnMountError> {
    match validate_mount_options(mount_options)? {
        None => Ok(()),
        Some(validated_mount_options) => {
            unmount(validated_mount_options.mount, UnmountFlags::DETACH)
                .map_err(io_to_unmount_error)?;
            Ok(())
        }
    }
}

struct StorageMount {
    mount: PathBuf,
    storage: PathBuf,
}

// in theory clap has already done this validation, but sadly they are still optional so I'm double checking everything is good here
// So here bundles them together into 1 option, idk how useful this is tbh
fn validate_mount_options(mount_options: &CliMount) -> Result<Option<StorageMount>, OptionsError> {
    match mount_options.storage_device.clone() {
        Some(storage_device) => match mount_options.mount_point.clone() {
            Some(mount_point) => Ok(Some(StorageMount {
                storage: storage_device,
                mount: mount_point,
            })),
            None => {
                error!("Mount point set but storage device not, please specify storage device");
                Err(OptionsError::MountSetStorageMissing)
            }
        },
        None => match mount_options.mount_point {
            Some(_) => {
                error!("Storage device set but mount point not not, please specify mount point");
                Err(OptionsError::StorageSetMountMissing)
            }
            None => Ok(None),
        },
    }
}
