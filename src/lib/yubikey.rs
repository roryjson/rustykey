use log::{error, info};
use yubico_manager;

use yubico_manager::config::{Config, Mode, Slot};
use yubico_manager::hmacmode::Hmac;

#[derive(Debug)]
pub enum Error {
    Misc(String),
    NotFound(MissingKey),
    InvalidChallenge(String),
}

#[derive(Debug)]
pub enum MissingKey {
    NoKey(String),
    IncorrectKey(String),
}

pub fn chal_resp_yubikey(challenge: &[u8], slot: Slot) -> Result<Hmac, Error> {
    let mut yubi = yubico_manager::Yubico::new();

    if let Ok(device) = yubi.find_yubikey() {
        info!(
            "Yubikey found with Vendor ID: {:?}, Product ID {:?}",
            device.vendor_id, device.product_id
        );

        let config = Config::default()
            .set_vendor_id(device.vendor_id)
            .set_product_id(device.product_id)
            .set_variable_size(true)
            .set_mode(Mode::Sha1)
            .set_slot(slot);

        // Challenge can not be greater than 64 bytes
        let challenge_bytes = challenge;
        if challenge_bytes.len() > 64 {
            error!("Challenge was too long, needs to be 64 bytes or less");
            return Err(Error::InvalidChallenge(
                "Challenge was too long, needs to be 64 bytes or less".to_owned(),
            ));
        }
        // In HMAC Mode, the result will always be the SAME for the SAME provided challenge
        let hmac_result = match yubi.challenge_response_hmac(challenge_bytes, config) {
            Ok(result) => result,
            Err(err) => {
                error!("Failed challenge response. Additional info: {}", err);
                return Err(Error::Misc(format!(
                    "Failed challenge response. Additional info: {}",
                    err
                )));
            }
        };
        // let v: &[u8] = hmac_result.deref();

        Ok(hmac_result)
    } else {
        error!("Could not detect any Yubikey");
        Err(Error::NotFound(MissingKey::NoKey(
            "Could not detect any Yubikey".to_string(),
        )))
    }
}
//
// // Not the best unit tests as they require a yubikey plugged in or not depending on test
// #[cfg(test)]
// mod tests {
//     use super::*;
//
//     // only works with Sean's yubikey plugged in
//     #[test]
//     fn test_chal_resp_yubikey_working() {
//         let expected_result = "98b212e6e7a258f6096c9694cab7716c17364634";
//
//         let response = chal_resp_yubikey("mychallenge", Slot::Slot1);
//
//         assert!(response.is_ok());
//         assert_eq!(response.ok().unwrap(), expected_result);
//     }
//
//     #[test]
//     fn test_chal_resp_yubikey_64_bytes() {
//         let expected_result = "be87d187d99d67513400a2a1777be4e52bb434ba";
//
//         let response = chal_resp_yubikey(
//             "1234567890123456789012345678901234567890123456789012345678901234",
//             Slot::Slot1,
//         );
//
//         assert!(response.is_ok());
//         assert_eq!(response.ok().unwrap(), expected_result);
//     }
//
//     #[test]
//     fn test_chal_resp_yubikey_65_bytes() {
//         let response = chal_resp_yubikey(
//             "12345678901234567890123456789012345678901234567890123456789012345",
//             Slot::Slot1,
//         );
//
//         assert!(response.is_ok());
//     }
//
//     #[test]
//     fn test_chal_resp_yubikey_missing_no_key() {
//         let expected_error_string = "Could not detect any Yubikey".to_string();
//
//         let response = chal_resp_yubikey("mychallenge", Slot::Slot1);
//         match response {
//             Err(error) => match error {
//                 Error::NotFound(veriant) => match veriant {
//                     MissingKey::NoKey(string) => assert_eq!(string, expected_error_string),
//                     _ => assert!(false),
//                 },
//                 _ => assert!(false),
//             },
//             _ => assert!(false),
//         }
//     }
// }
