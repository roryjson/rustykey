use crate::cryptsetup_caller::CrypsetupError;
use inquire::{Confirm, InquireError, Select};
use std::fmt::{Display, Formatter};
use std::path::{Path, PathBuf};

struct RecoveryOption {
    display_message: String,
    function_to_run: fn(
        cryptsetup_executable_path: &PathBuf,
        device: &Path,
        name: &str,
        in_initrd: bool,
        cryptsetup_args: &[String],
    ) -> Result<(), CrypsetupError>,
}

impl Display for RecoveryOption {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.display_message)
    }
}

pub fn recovery(
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
    name: &str,
    in_initrd: bool,
    cryptsetup_args: &[String],
) {
    let mut confirm = "Would you like to attempt a recovery mode?";

    'retry: loop {
        println!("Failed to unlock device with the above error.");
        let ans = Confirm::new(confirm).with_default(false).prompt();
        confirm = "Would you like to attempt another recovery mode?";

        match ans {
            Ok(true) => {
                let options: Vec<RecoveryOption> = vec![RecoveryOption {
                    display_message:
                        "Fallback password? (Don't use yubikey just pass password straight to LUKS)"
                            .to_string(),
                    function_to_run: crate::cryptsetup::open::straight_password,
                }];

                // let options: Vec<&str> = vec![
                //     "Fallback password? (Don't use yubikey just pass password straight to LUKS)",
                //     "Select different salt file? (Provide a new path to salt_iter file)",
                //     "Input salt and iterations manually?",
                //     "First install of rustykey? I can recover from a known bug in the old Nixos method", // todo this needs testing somehow... my laptop has got corrupted beyond recovery so cant test there easily, and a Vm would mean changing nixos versions part way which is complicated
                // ];

                let ans: Result<RecoveryOption, InquireError> =
                    Select::new("Which method would like like todo to recover?", options).prompt();

                match ans {
                    Ok(choice) => {
                        match (choice.function_to_run)(
                            cryptsetup_executable_path,
                            device,
                            name,
                            in_initrd,
                            cryptsetup_args,
                        ) {
                            Ok(_) => break 'retry,
                            Err(error) => match error {
                                CrypsetupError::IncorrectKey => continue 'retry,
                                _ => todo!(),
                            },
                        }
                    }
                    Err(_) => println!("There was an error."),
                }
            }
            Ok(false) => break 'retry,
            Err(_) => {
                println!("Failed to parse input, exiting");
                break 'retry;
            }
        }
    }
}
