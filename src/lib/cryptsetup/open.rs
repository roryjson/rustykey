use crate::cryptsetup_caller;
use crate::cryptsetup_caller::CrypsetupError;
use inquire::{Password, PasswordDisplayMode};
use std::path::{Path, PathBuf};

/// Method to ask user for a password to be passed directly to `cryptsetup luksOpen` with no changes.
/// This is typically used as a fallback option if a 2FA unlock has failed.
///
/// # Arguments
///
/// * `cryptsetup_executable_path`: Path to cryptsetup executable, to be called with `std::process::Command(cryptsetup_executable_path)`
/// * `device`: Path to block device we are unlocking
/// * `name`: Name to map unlocked block device to
///
/// returns: Result<(), CrypsetupError>
///
/// # Examples
///
/// ```no_run
/// # use std::path::PathBuf;
/// # use rustykey::cryptsetup_caller::CrypsetupError;
/// # use rustykey::cryptsetup::open::straight_password;
/// match straight_password(&PathBuf::from("cryptsetup"), &PathBuf::from("/dev/sda1"), "root", false, &[]) {
///     Ok(()) => println!("successful unlock"),
///     Err(error) => println!("Unlock failed with error: {:?}", error)
/// }
/// ```
pub fn straight_password(
    cryptsetup_executable_path: &PathBuf,
    device: &Path,
    name: &str,
    in_initrd: bool,
    cryptsetup_args: &[String],
) -> Result<(), CrypsetupError> {
    let input = Password::new("Please enter password:")
        .with_display_toggle_enabled()
        .without_confirmation()
        .with_display_mode(PasswordDisplayMode::Hidden)
        .with_formatter(&|_| String::from("Input received"))
        .prompt();

    let user_password = match input {
        Ok(val) => val,
        Err(_) => todo!(),
    };

    cryptsetup_caller::open(
        user_password.as_bytes(),
        cryptsetup_executable_path,
        device,
        name,
        in_initrd,
        cryptsetup_args,
    )?;

    Ok(())
}
