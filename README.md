# RustyKey

Welcome to RustyKey. An application to help manage and use LUKS keys generated with input from a Yubikey.
At the moment I am working towards an [MVP](https://gitlab.com/seam345/rustykey/-/milestones/1) at which point I plan to
attempt to upstream this method into Nixos for unlocking with a Yubikey.

## Goals
- [ ] Simple way to add generated keys to LUKS
- [ ] A way for the tool to self install or display a guide to users on how to install into initrd
  - [ ] Guide
  - [ ] Self install
- [ ] Generate audit documentation for every release

## Requests for feedback

Issues that I would like some more input/ideas on. No coding necessary

### Making some kind of audit document for each release https://gitlab.com/seam345/rustykey/-/issues/14

This is something I have never done but feels like a great thing to have in a security tool

### Default options https://gitlab.com/seam345/rustykey/-/issues/15

While working on MVP I'm sure to hit a bunch of possible user options, and would like feedback on which should be default
