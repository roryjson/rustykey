let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };

in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable."1.68.0".default.override { extensions = [ "rust-src" ]; })
    
    # Add some extra dependencies from `pkgs`
    openssl # for sha2 digest
    # for block-utils, to mount the device with the salt on
    systemd
    pkg-config

    # for auditing
    cargo-deny
    cargo-audit
    cargo-auditable

    # spell checker
    typos
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}


